package com.nerdscorner.mvpsample.bus;

import com.squareup.otto.Bus;

public class BusProvider {
    private static Bus BUS;

    public static Bus getInstance() {
        if (BUS == null) {
            BUS = new Bus();
        }
        return BUS;
    }

    public static void register(Object object) {
        BUS.register(object);
    }

    public static void unregister(Object object) {
        BUS.unregister(object);
    }
}
