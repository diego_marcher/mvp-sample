package com.nerdscorner.mvpsample.domain;

public class DemoItem {
    private final String value;

    public DemoItem(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
