package com.nerdscorner.mvpsample.ui.mvp.view;

import android.app.Activity;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.lang.ref.WeakReference;

public class ActivityView {
    private WeakReference<AppCompatActivity> activityRef;

    ActivityView(AppCompatActivity activity) {
        activityRef = new WeakReference<>(activity);
    }

    @Nullable
    public AppCompatActivity getActivity() {
        return activityRef.get();
    }

    public void showToast(@StringRes int textResId) {
        Activity activity = getActivity();
        if (activity == null) {
            return;
        }
        showToast(activity.getText(textResId));
    }

    public void showToast(CharSequence text) {
        Activity activity = getActivity();
        if (activity == null) {
            return;
        }
        Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
    }
}
