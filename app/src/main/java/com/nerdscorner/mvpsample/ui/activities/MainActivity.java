package com.nerdscorner.mvpsample.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.nerdscorner.mvpsample.R;
import com.nerdscorner.mvpsample.bus.BusProvider;
import com.nerdscorner.mvpsample.ui.mvp.model.MainModel;
import com.nerdscorner.mvpsample.ui.mvp.presenter.MainPresenter;
import com.nerdscorner.mvpsample.ui.mvp.view.MainView;
import com.squareup.otto.Bus;

public class MainActivity extends AppCompatActivity {
    private MainPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Bus bus = BusProvider.getInstance();
        presenter = new MainPresenter(
                new MainView(this, bus),
                new MainModel(bus)
        );
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.register(presenter);
        presenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.unregister(presenter);
    }
}
