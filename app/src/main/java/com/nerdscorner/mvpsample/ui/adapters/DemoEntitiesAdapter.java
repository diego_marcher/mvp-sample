package com.nerdscorner.mvpsample.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nerdscorner.mvpsample.R;
import com.nerdscorner.mvpsample.domain.DemoItem;
import com.squareup.otto.Bus;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.view.LayoutInflater.from;

public class DemoEntitiesAdapter extends Adapter<DemoEntitiesAdapter.ViewHolder> {

    private final List<DemoItem> entities;
    private final Bus bus;

    public DemoEntitiesAdapter(List<DemoItem> entities, Bus bus) {
        this.entities = entities;
        this.bus = bus;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(from(parent.getContext()).inflate(R.layout.demo_entity_row, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final DemoItem demoItem = entities.get(position);
        holder.text.setText(demoItem.getValue());
        holder.itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                bus.post(new ItemClickEvent(demoItem));
            }
        });
    }

    @Override
    public int getItemCount() {
        return entities.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.text) TextView text;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public static class ItemClickEvent {
        public final DemoItem demoItem;

        ItemClickEvent(DemoItem demoItem) {
            this.demoItem = demoItem;
        }
    }
}