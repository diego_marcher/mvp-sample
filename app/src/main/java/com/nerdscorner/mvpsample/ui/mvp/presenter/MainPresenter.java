package com.nerdscorner.mvpsample.ui.mvp.presenter;

import android.app.Activity;
import android.content.Intent;

import com.nerdscorner.mvpsample.R;
import com.nerdscorner.mvpsample.ui.activities.ItemActivity;
import com.nerdscorner.mvpsample.ui.adapters.DemoEntitiesAdapter;
import com.nerdscorner.mvpsample.ui.adapters.DemoEntitiesAdapter.ItemClickEvent;
import com.nerdscorner.mvpsample.ui.mvp.model.MainModel;
import com.nerdscorner.mvpsample.ui.mvp.model.MainModel.LoadFailedEvent;
import com.nerdscorner.mvpsample.ui.mvp.model.MainModel.LoadSuccessEvent;
import com.nerdscorner.mvpsample.ui.mvp.view.MainView;
import com.squareup.otto.Subscribe;

public class MainPresenter {
    private final MainView view;
    private final MainModel model;

    public MainPresenter(MainView view, MainModel model) {
        this.view = view;
        this.model = model;
    }

    @Subscribe
    public void onLoadSuccess(LoadSuccessEvent event) {
        view.setAdapter(new DemoEntitiesAdapter(model.getDemoItems(), model.getBus()));
    }

    @Subscribe
    public void onLoadFailed(LoadFailedEvent event) {
        view.showToast(R.string.algo_salio_mal);
    }

    @Subscribe
    public void onItemClick(ItemClickEvent event) {
        Activity activity = view.getActivity();
        if (activity == null) {
            return;
        }
        activity.startActivity(new Intent(activity, ItemActivity.class));
    }

    public void onResume() {
        model.fetchDemoItems();
    }
}
