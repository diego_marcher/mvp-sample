package com.nerdscorner.mvpsample.ui.mvp.model;

import android.os.AsyncTask;

import com.nerdscorner.mvpsample.domain.DemoItem;
import com.squareup.otto.Bus;

import java.util.LinkedList;
import java.util.List;

public class MainModel {

    private final Bus bus;
    private List<DemoItem> demoItems;

    public MainModel(Bus bus) {
        this.bus = bus;
    }

    public void fetchDemoItems() {
        new AsyncTask<Void, Integer, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... voids) {
                /** Server Call **/
                demoItems = new LinkedList<>();
                demoItems.add(new DemoItem("Item 1"));
                demoItems.add(new DemoItem("Item 2"));
                demoItems.add(new DemoItem("Item 3"));
                return true;
            }

            @Override
            protected void onPostExecute(Boolean success) {
                if (success) {
                    bus.post(new LoadSuccessEvent());
                } else {
                    bus.post(new LoadFailedEvent());
                }
            }
        }.execute();
    }

    public List<DemoItem> getDemoItems() {
        return demoItems;
    }

    public Bus getBus() {
        return bus;
    }

    public static class LoadSuccessEvent {
    }

    public static class LoadFailedEvent {
    }
}
