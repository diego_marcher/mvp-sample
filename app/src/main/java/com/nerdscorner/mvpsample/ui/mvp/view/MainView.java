package com.nerdscorner.mvpsample.ui.mvp.view;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

import com.nerdscorner.mvpsample.R;
import com.nerdscorner.mvpsample.ui.adapters.DemoEntitiesAdapter;
import com.squareup.otto.Bus;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainView extends ActivityView {

    private final Bus bus;
    @Bind(R.id.list) RecyclerView list;

    public MainView(AppCompatActivity activity, Bus bus) {
        super(activity);
        this.bus = bus;
        ButterKnife.bind(this, activity);
        list.setLayoutManager(new LinearLayoutManager(activity));
    }

    public void setAdapter(DemoEntitiesAdapter adapter) {
        list.setAdapter(adapter);
        list.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent e) {
                bus.post(new ListTouchedEvent());
                return false;
            }
        });
    }

    public static class ListTouchedEvent {
    }
}
